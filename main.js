import App from './App'
import { $http } from '@escook/request-miniprogram'
uni.$http = $http
// 配置请求根路径
$http.beforeRequest=function(options){
  uni.showLoading({
    title:'数据加载中'
  })
}
$http.afterRequest = function () {
  uni.hideLoading()
}
$http.baseUrl = 'https://api-ugo-web.itheima.net'
uni.$showMsg = function (title = '数据加载失败！', duration = 1500) {
  uni.showToast({
    title,
    duration,
    icon: 'none',
  })
}
// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif